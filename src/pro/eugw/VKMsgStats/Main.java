package pro.eugw.VKMsgStats;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.Objects;

public class Main {
    private static String token() throws IOException {
        return new BufferedReader(new FileReader(new File("token.file"))).readLine();
    }

    public static void main(String[] args) throws Exception {
        File index = new File("index.html");
        File fin_file = new File(new BufferedReader(new FileReader(new File("final.file"))).readLine());
        ArrayList<String> array = writer();
        PrintWriter pw = new PrintWriter(index);
        pw.println("<html lang=\"ru\"><head>\n" +
                "    <meta charset=\"utf-8\">\n" +
                "    <meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\">\n" +
                "    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">\n" +
                "    <link href=\"lib/css/bootstrap.css\" rel=\"stylesheet\" type=\"text/css\">\n" +
                "</head>\n" +
                "<body id=\"root\"><div data-reactroot=\"\"><div class=\"container\"><div class=\"row\"><table class=\"table table-hover\"><thead><tr><th>Имя</th><th class=\"text-left\">Всего</th><th class=\"text-left\">Отправлено</th><th class=\"text-left\">Получено</th></tr></thead><tbody>");
        for (String mr : array) {
            String name = null;
            if (mr.split(":")[4].equals("u")) {
                JsonArray jsonArray = get("users.get", "user_id=" + mr.split(":")[0]);
                name = jsonArray.get(0).getAsJsonObject().get("first_name").getAsString() + " " + jsonArray.get(0).getAsJsonObject().get("last_name").getAsString();
            }
            if (mr.split(":")[4].equals("c")) {
                JsonArray jsonArray = get("messages.getChat", "access_token=" + token() + "&chat_id=" + mr.split(":")[0]);
                name = jsonArray.get(0).getAsJsonObject().get("title").getAsString();
            }
            pw.println("<tr><td><i>" + name + "</i></td><td>" + mr.split(":")[1] + "</td><td>" + mr.split(":")[2] + "</td><td>" + mr.split(":")[3] + "</td></tr>");
        }
        pw.println("</tbody></table></div></div></div></body></html>");
        pw.println();
        pw.flush();
        pw.close();
        if (fin_file.exists()) fin_file.delete();
        Files.copy(index.toPath(), fin_file.toPath());
    }

    private static JsonArray get(String method, String arguments) throws Exception {
        while (true) {
            URL url = new URL("https://api.vk.com/method/" + method + "?" + arguments);
            System.out.println(url);
            HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
            urlConnection.setRequestMethod("GET");
            JsonObject object = new JsonParser().parse(new BufferedReader(new InputStreamReader(urlConnection.getInputStream())).readLine()).getAsJsonObject();
            if (object.has("response")) {
                if (object.get("response").isJsonArray()) {
                    return object.getAsJsonArray("response");
                }
                if (object.get("response").isJsonObject()) {
                    JsonArray jsonArray = new JsonArray();
                    jsonArray.add(object.getAsJsonObject("response"));
                    return jsonArray;
                }
            }
        }
    }

    private static ArrayList<ArrayList<Integer>> ids() throws Exception {
        Integer count = get("messages.getDialogs", "access_token=" + token()).get(0).getAsInt();
        ArrayList<ArrayList<Integer>> array = new ArrayList<>();
        array.add(new ArrayList<>());
        array.add(new ArrayList<>());
        for (Integer i = 0; i < count; i = i + 200) {
            JsonArray jsonArray = get("messages.getDialogs", "access_token=" + token() + "&offset=" + i + "&count=200");
            jsonArray.remove(0);
            for (JsonElement eer : jsonArray) {
                if (!eer.getAsJsonObject().has("chat_id")
                        && eer.getAsJsonObject().get("uid").getAsInt() > 0) {
                    array.get(0).add(eer.getAsJsonObject().get("uid").getAsInt());
                }
                if (eer.getAsJsonObject().has("chat_id")) {
                    array.get(1).add(eer.getAsJsonObject().get("chat_id").getAsInt());
                }
            }
        }
        return array;
    }

    private static ArrayList<Integer> chat_counter(Integer chat_id) throws Exception {
        Integer count = get("messages.getHistory", "access_token=" + token() + "&chat_id=" + chat_id).get(0).getAsInt();
        ArrayList<Integer> cnt = new ArrayList<>();
        Integer outcount = 0;
        Integer incount = 0;
        cnt.add(0);
        cnt.add(0);
        for (Integer i = 0; i < count; i = i + 200) {
            JsonArray jsonArray = get("messages.getHistory", "access_token=" + token() + "&chat_id=" + chat_id + "&offset=" + i + "&count=200");
            jsonArray.remove(0);
            for (JsonElement eer : jsonArray) {
                if (eer.getAsJsonObject().get("out").getAsInt() == 1) {
                    outcount++;
                    cnt.set(1, outcount);
                } else {
                    incount++;
                    cnt.set(0, incount);
                }
            }
        }
        return cnt;
    }

    private static ArrayList<Integer> user_counter(Integer user_id) throws Exception {
        Integer count = get("messages.getHistory", "access_token=" + token() + "&user_id=" + user_id).get(0).getAsInt();
        ArrayList<Integer> cnt = new ArrayList<>();
        Integer outcount = 0;
        Integer incount = 0;
        cnt.add(0);
        cnt.add(0);
        for (Integer i = 0; i < count; i = i + 200) {
            JsonArray jsonArray = get("messages.getHistory", "access_token=" + token() + "&user_id=" + user_id + "&offset=" + i + "&count=200");
            jsonArray.remove(0);
            for (JsonElement eer : jsonArray) {
                if (eer.getAsJsonObject().get("out").getAsInt() == 1) {
                    outcount++;
                    cnt.set(1, outcount);
                } else {
                    incount++;
                    cnt.set(0, incount);
                }

            }
        }
        return cnt;
    }

    private static ArrayList<String> writer() throws Exception {
        ArrayList<String> arrayList = new ArrayList<>();
        ArrayList<Thread> threads = new ArrayList<>();
        for (Integer id : ids().get(0)) {
            Runnable task = () -> {
                ArrayList<Integer> array = null;
                try {
                    array = user_counter(id);
                } catch (Exception e) {
                    e.printStackTrace();
                    System.exit(0);
                }
                assert array != null;
                Integer sum = array.get(0) + array.get(1);
                Integer out = array.get(1);
                Integer in = array.get(0);
                arrayList.add(id + ":" + sum + ":" + out + ":" + in + ":u");
            };
            Thread worker = new Thread(task);
            worker.setName(String.valueOf(id));
            worker.start();
            threads.add(worker);
        }
        for (Integer id : ids().get(1)) {
            Runnable task = () -> {
                ArrayList<Integer> array = null;
                try {
                    array = chat_counter(id);
                } catch (Exception e) {
                    e.printStackTrace();
                    System.exit(0);
                }
                assert array != null;
                Integer sum = array.get(0) + array.get(1);
                Integer out = array.get(1);
                Integer in = array.get(0);
                arrayList.add(id + ":" + sum + ":" + out + ":" + in + ":c");
            };
            Thread worker = new Thread(task);
            worker.setName(String.valueOf(id));
            worker.start();
            threads.add(worker);
        }
        Integer running;
        do {
            running = 0;
            for (Thread thread : threads) {
                if (thread.isAlive()) {
                    running++;
                }
            }
        } while (running > 0);
        ArrayList<String> fin = new ArrayList<>();
        for (Integer j = 0; j < arrayList.size(); j++) {
            Integer max = 0;
            for (String anArrayList : arrayList) {
                if (Integer.valueOf(anArrayList.split(":")[1]) > max) {
                    max = Integer.valueOf(anArrayList.split(":")[1]);
                }
            }
            for (Integer i = 0; i < arrayList.size(); i++) {
                if ((Objects.equals(Integer.valueOf(arrayList.get(i).split(":")[1]), max) && (!arrayList.get(i).equals("0:0:0:0:0")))) {
                    fin.add(arrayList.get(i));
                    arrayList.set(i, "0:0:0:0:0");
                }
            }
        }
        return fin;
    }
}